with Ada.Command_Line ; 
with puzzle ;
procedure sudprint is
   fn : string := Ada.Command_Line.Argument(1) ;
   p : puzzle.Squares_Type ;
begin
   p := puzzle.Load(fn);
   puzzle.Pretty(p , fn & ".pretty"); 
end sudprint;
