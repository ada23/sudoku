package puzzle.classic is

   function Valid( p : Squares_Type ; diagonal : boolean := false ) return boolean ;
   procedure Solve( p : in out Squares_Type ;
                    attempts : in out integer ;
                    maxattempts : integer := 10000 ;
                    diagonal : boolean := false ) ;
   
   procedure Compose( p : out Squares_Type ;
                      ps : out Squares_Type ;
                      attempts : out Integer ) ;
   
end puzzle.classic;
