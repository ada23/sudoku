with Ada.Text_Io; use Ada.Text_Io;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded ;
with Ada.Command_Line ; use Ada.Command_Line ;
with puzzle ;
with puzzle.classic ;

procedure Sudoku is
   P : Puzzle.Squares_Type;
   tries : integer := 0 ;
   maxtries : integer := 100000 ;
   fn : Unbounded_String := To_Unbounded_String("puzzle1.txt");
   ofn : Unbounded_String := To_Unbounded_String("puzzle1.solution");
   diag : boolean := false ;
begin
   if Argument_Count > 0
   then
      fn := To_Unbounded_String( Argument(1) ) ;
      ofn := To_Unbounded_String( Argument(1) & ".solution" ) ;
   end if;

   if Argument_Count > 1
   then
      diag := true ;
   end if ;

   p := Puzzle.Load(to_String(fn));
   if not puzzle.classic.Valid(p,diag)
   then
      Put(to_string(fn)) ; Put_LIne(" is not a valid puzzle");
      return ;
   end if ;

   puzzle.classic.Solve(p,tries,maxtries,diag) ;
   Puzzle.Save(p,to_String(ofn));
   Put("Solution : "); Put(to_String(ofn));
   Put(" Solved ");
   Put(boolean'Image(puzzle.Solved(p))) ;
   Put(" Tries : ");
   Put(Integer'image(tries));
   New_Line ;
end Sudoku;
