with Text_io; use Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_IO ;
with GNAT.Random_Numbers ;

package body puzzle.classic is

   
   function Valid( p : Squares_Type ; diagonal : boolean := false ) return boolean is
   begin
      for row in 1..9
      loop
         if not RowValid(p,row)
         then
            return false ;
         end if ;
      end loop ;
      for col in 1..9
      loop
         if not ColValid(p,col)
         then
            return false ;
         end if ;
      end loop ;
      for subsqr in 1..9
      loop
         for subsqc in 1..9
         loop
            if not SubSqValid(p,subsqr,subsqc)
            then
               return false ;
            end if ;
         end loop ;
      end loop ;

      return true ;
   end Valid ;
   
   procedure Solve( p : in out Squares_Type ;
                    attempts : in out integer ;
                    maxattempts : integer := 10000 ;
                    diagonal : boolean := false ) is
      
      r,c : integer := 0 ;
      function PotentialFit (row, col : idx_type ; sq : Square_Type ) return boolean is
      begin
         --  Put("Trying ");
         --  Put(row);
         --  Put(col);
         --  Put(integer(sq));
         --  New_Line ;
         if RowOk(p,row,sq) and ColOK(p,col,sq) and SubSqOk(p, row , col,sq)
         then
            if not diagonal
            then
               return true ;
            end if ;
            if row = col
            then
               for r in 1..9
               loop
                  if p(r,r) = sq
                  then
                     return false ;
                  end if ;
               end loop ;
            end if ;
            
            if row = 10 - col
            then
               for r in 1..9
               loop
                  if p(r,10-r) = sq
                  then
                     return false ;
                  end if ;
               end loop ;
            end if ;
            return true ;
         end if ;
         return false ;
      end PotentialFit ;
      
   begin
      for row in 1..9
      loop
         for col in 1..9
         loop
            if Blank(p,row,col)
            then
               r := row ;
               c := col ;
               exit ;
            end if ;
         end loop ;
         if r /= 0
         then
            exit ;
         end if ;
      end loop ;
      if r = 0 or c = 0
      then
         return ;
      end if ;
      
      for sq in Square_TYpe'first+1..Square_TYpe'last
      loop
         if PotentialFit(r,c,sq) -- RowOk(p,r,sq) and ColOk(p,c,sq) and SubSqOk(p,r,c,sq)
         then
            p(r,c) := sq ;
            attempts := attempts + 1 ;
            Solve( p , attempts , maxattempts , diagonal ) ;
            if Solved(p)
            then
               return ;
            end if ;
            if attempts >= maxattempts
            then
               return ;
            end if ;

            p(r,c) := 0 ;
         end if ;
      end loop ;
   end Solve;
  
   gen : GNAT.Random_Numbers.Generator ;
   procedure Compose( p : out Squares_Type ;
                      ps : out Squares_Type ;
                      attempts : out Integer ) is
      count : integer := 0 ;
      loc : integer ;
      
      procedure Split( num : integer ; row, col, sq : out integer ) is
         temp : integer := num rem 1000 ;
      begin
         row := -1 ; col := -1 ;
         
         sq := temp rem 10 ;
         if sq = 0 
         then
            return ;
         end if ;

         col := temp / 10 ;
         if col rem 10 = 0 
         then
            return ;
         end if ;
         if col / 10 = 0
         then
            return ;
         end if ;
         row := col rem 10 ;
         col := col / 10 ;
         Put("Returning "); Put(row); Put(col); Put(sq); New_Line ;
      end Split ;
      r, c, s : integer ;
      covered : integer ;
   begin
      attempts := 0 ;
      p := (others => (others => 0));
      ps := p ;
      GNAT.Random_Numbers.Reset(gen) ;

      while count = 0
      loop
         count := GNAT.Random_Numbers.Random(gen);
         count := count rem 31 ;
      end loop ;
      
      Put("Will cover ");Put(count); Put_Line(" squares");
      covered := 0 ;
      while covered < count
      loop
         loc := GNAT.Random_Numbers.Random(gen) ;
         Split(loc,r,c,s) ;
         if r > 0
         then
            p(r,c) := square_type(s) ;
            if Valid(p)
            then
               covered := covered + 1 ;
            else
               p(r,c) := 0;
            end if ;
         end if ;
      end loop ;
      
      ps := p ;
      Solve(ps,attempts) ;
   end Compose ;
   
   
end puzzle.classic;
