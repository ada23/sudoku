with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with puzzle.classic ;

procedure gensudoku is
   puz, puzsolved : puzzle.Squares_Type ;
   complexity : integer ;
begin
   puzzle.classic.Compose(puz,puzsolved,complexity);
   puzzle.Save(puz,"composed1.txt");
   Puzzle.Save(puzsolved,"composed1.solution");
   Put("Solution : "); Put("composed1.solution");
   Put(" Solved ");
   Put(boolean'Image(puzzle.Solved(puzsolved))) ;
   Put(" Tries : ");
   Put(Integer'image(complexity));
   New_Line ;
   
end gensudoku;
