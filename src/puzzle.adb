with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
package body puzzle is

   package Square_Text_Io is new Ada.Text_Io.Integer_IO(Square_Type);
   use Square_Text_Io;
   
   function Load(fn : string) return Squares_Type is
      result : Squares_Type ;
      puzfile : File_Type ;
      char : character ;
   begin
      Open(puzfile,In_File,fn);
      for row in 1..9
      loop
         for col in 1..9
         loop
            Get(puzfile,result(row,col));
            char := ASCII.Nul ;
            if col < 9
            then
               while char /= ','
               loop
                  Get(puzfile,char);
               end loop ;
            else
               Skip_Line(puzfile);
            end if ;
         end loop ;
      end loop ;
      Close(puzfile) ;
      return result ;
   end Load ;
   
   procedure Save(sq : Squares_Type; fn : string ) is
      puzfile : File_Type ;
   begin
      Create(puzfile,Out_File,fn);
      for row in 1..9
      loop
         for col in 1..9
         loop
            Put(puzfile,Square_Type'Image(sq(row,col)));
            if col < 9
            then
               Put(puzfile," , ");
            else
               New_Line(puzfile) ;
            end if ;
         end loop ;
      end loop ;
      Close(puzfile) ;
   end Save ;
   
   procedure Pretty( sq : Squares_Type ; fn : string ) is
   begin
      Put_Line("+----------------------------------+") ;
      for row in 1..9
      loop
         Put("|");
         for col in 1..9
         loop
            if sq(row,col) = 0
            then
               Put(" . ");
            else
               Put(sq(row,col));
               Put(" ");
            end if ; 
            if col rem 3 = 0
            then
               Put(" | ");
            end if ;
         end loop ;
         New_Line ;
         if row rem 3 = 0
         then
            Put_Line("+----------------------------------+") ;
         end if ;
         
      end loop ;
   end Pretty ;
   
   procedure SubPuzOrigin( row : idx_type ; col : idx_type ;
                           subrow : out idx_type ; subcol : out idx_type ) is
      function SubPuzOrigin( idx : idx_type ) return idx_type is
      begin
         if idx in 1..3
         then
            return 1 ;
         elsif idx in 4..6
         then
            return 4 ;
         elsif idx in 7..9
         then
            return 7 ;
         end if ;
         raise Program_Error ;
         end SubPuzOrigin ;
   begin
      subrow := SubPuzOrigin(row);
      subcol := SubPuzOrigin(col);
   end SubPuzOrigin ;
   function Solved( p : Squares_Type ) return boolean is
   begin
      for row in 1..9
      loop
         for col in 1..9
         loop
            if Blank(p,row,col)
            then
               return false ;
            end if ;
         end loop ;
      end loop ;
      return true ;
   end Solved ;
   
   function Blank( p : Squares_Type ; row : idx_type ; col : idx_type ) return boolean is
   begin
      return p(row,col) = 0 ;
   end Blank ;
        
   function RowOk( p : Squares_Type ; row : idx_type ; sq : Square_Type ) return boolean is
   begin
      for col in 1..9
      loop
         if p(row,col) = sq
         then
            return false ;
         end if ;
      end loop ;
      return true ;
   end RowOk ;
   
   function ColOk( p : Squares_Type ; col : idx_type ; sq : Square_Type ) return boolean is
   begin
      for row in 1..9
      loop
         if p(row,col) = sq
         then
            return false ;
         end if ;
      end loop ;
      return true ;
   end ColOk ;
   function DiagOk( p : Squares_Type ; sq : Square_Type ) return boolean is
   begin
      for r in 1..9
      loop
         if p(r,r) = sq
         then
            Put("Diag Ok false (Row1) :"); Put(r) ; Put(" "); Put(p(r,r)) ; Put(sq) ; New_Line ;
            return false ;
         end if ;
         if p(r,10-r) = sq
         then
            Put("Diag Ok false (10 - r) :"); Put(10-r) ; Put(" "); Put(p(r,10-r)) ; Put(sq) ; New_Line ;
            return false ;
         end if ;
      end loop ;
      return true ;
   end DiagOk ;
   function SubSqOk( p : Squares_Type ;  row : idx_type ; col : idx_type ; sq : Square_Type ) return boolean is
      subrow : idx_type ;
      subcol : idx_type ;
   begin
      SubPuzOrigin( row , col , subrow , subcol ) ;
      for r in 1..3
      loop
         for c in 1..3
         loop
            if p( subrow + r - 1 , subcol + c - 1 ) = sq
            then
               return false ;
            end if ;
         end loop ;
      end loop ;
      return true ;
   end SubSqOk ;
   
   function RowValid( p : Squares_Type ; row : idx_type ) return boolean is
      seen : array (Square_Type'range) of boolean := (others => false) ;
   begin
      for c in 1..9
      loop
         if p(row,c) /= 0
         then
            if seen( p(row,c) )
            then
               return false ;
            end if ;
            seen( p(row,c) ) := true ;
         end if ;
      end loop ;
      return true ;
   end RowValid ;
   
   function ColValid( p : Squares_Type ; col : idx_type ) return boolean is
      seen : array (Square_TYpe'range) of boolean := (others => false) ;
   begin
      for r in 1..9
      loop
         if p(r,col) /= 0
         then
            if seen( p(r,col) )
            then
               return false ;
            end if ;
            seen( p(r,col) ) := true ;
         end if ;
      end loop ;
      return true ;
   end ColValid ;
   function SubSqValid( p : Squares_Type ; row : idx_type ; col : idx_type ) return boolean is
      seen : array (Square_TYpe'range) of boolean := (others => false) ;
      srow, scol : idx_type ;
   begin
      SubPuzOrigin(row,col,srow,scol) ;
      for r in 1..3
      loop
         for c in 1..3
         loop
            if p(r,c) /= 0
            then
               if seen( p(r,c) )
               then
                  return false ;
               end if ;
            end if ;
         end loop ;
      end loop ;
      return true ;
   end SubSqValid ;
   
   function DiagValid( p : Squares_Type ) return boolean is
       seen : array (Square_TYpe'range) of boolean := (others => false) ;
   begin
      for r in 1..9
      loop
         if p(r,r) /= 0
         then
            if seen( p(r,r) )
            then
               return false ;
            end if ;
            seen( p(r,r) ) := true ;
         end if ;
      end loop ;
      seen := (others => false) ;
      for r in 1..9
      loop
         if p(r,9-r+1) /= 0
         then
            if seen( p(r,9-r+1) )
            then
               return false ;
            end if ;
         end if ;
      end loop ;
      return true ;
   end DiagValid ;
   
   
end puzzle ;
