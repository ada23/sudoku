package puzzle is
   type square_type is new integer range 0..9 ;
   type Squares_Type is array (1..9,1..9) of square_type ;
   subtype Idx_Type is integer range 1..9 ;
   
   function Load(fn : string) return Squares_Type ;
   procedure Save(sq : Squares_Type; fn : string ) ;
   procedure Pretty( sq : Squares_Type ; fn : string ) ;
   
   procedure SubPuzOrigin( row : idx_type ; col : idx_type ;
                           subrow : out idx_type ; subcol : out idx_type ) ;
   
   function RowValid( p : Squares_Type ; row : idx_type ) return boolean ;
   function ColValid( p : Squares_Type ; col : idx_type ) return boolean ;
   function SubSqValid( p : Squares_Type ; row : idx_type ; col : idx_type ) return boolean ;
   function DiagValid( p : Squares_Type ) return boolean ;
   
   function Solved( p : Squares_Type ) return boolean ;
   function Blank( p : Squares_Type ; row : idx_type ; col : idx_type ) return boolean ;
   function RowOk( p : Squares_Type ; row : idx_type ; sq : Square_Type ) return boolean ;
   function ColOk( p : Squares_Type ; col : idx_type ; sq : Square_Type ) return boolean ;
   function SubSqOk( p : Squares_Type ;  row : idx_type ; col : idx_type ; sq : Square_Type ) return boolean ;
   function DiagOk( p : Squares_Type ; sq : Square_Type ) return boolean ;
   
   
   
end puzzle ;
